#ifndef GRADECALCULATOR_H
#define GRADECALCULATOR_H

#include <QMainWindow>

namespace Ui {
class GradeCalculator;
}

class GradeCalculator : public QMainWindow
{
    Q_OBJECT

public:
    explicit GradeCalculator(QWidget *parent = 0);
    ~GradeCalculator();

private slots:
    void change_hw1(int);
    void change_hw2(int);
    void change_hw3(int);
    void change_hw4(int);
    void change_hw5(int);
    void change_hw6(int);
    void change_hw7(int);
    void change_hw8(int);
    void change_mid1(int);
    void change_mid2(int);
    void change_final(int);

private:
    Ui::GradeCalculator *ui;
    void change_label() const;
    int hw1_val;
    int hw2_val;
    int hw3_val;
    int hw4_val;
    int hw5_val;
    int hw6_val;
    int hw7_val;
    int hw8_val;
    int mid1_val;
    int mid2_val;
    int final_val;
};

#endif // GRADECALCULATOR_H
