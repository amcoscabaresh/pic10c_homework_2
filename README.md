# PIC 10C Homework 2 #

This repository contains my second homework assignment for PIC 10C. It is a grade calculator made using Qt which allows me to make "prettier" graphics than with C++.


### How To Operate ###

The calculator has eleven sliding bars and corresponding spin boxes. They are labeled as Hw 1-8, Midterm 1, Midterm 2, and Final. The user can use the sliding bar, spin box, or both to select their scores on each assignment.
The player then has the option to select which grading scheme they would like to use. The default is grading scheme 1 which includes both of the midterm grades.
For both grading schemes, the lowest homework score is not counted.