#include "gradecalculator.h"
#include "ui_gradecalculator.h"

GradeCalculator::GradeCalculator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GradeCalculator),
    hw1_val(0),
    hw2_val(0),
    hw3_val(0),
    hw4_val(0),
    hw5_val(0),
    hw6_val(0),
    hw7_val(0),
    hw8_val(0),
    mid1_val(0),
    mid2_val(0),
    final_val(0)

{
    ui->setupUi(this);
    QObject::connect( ui->horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(change_hw1(int)));
    QObject::connect( ui->horizontalSlider_2, SIGNAL(valueChanged(int)), this, SLOT(change_hw2(int)));
    QObject::connect( ui->horizontalSlider_3, SIGNAL(valueChanged(int)), this, SLOT(change_hw3(int)));
    QObject::connect( ui->horizontalSlider_4, SIGNAL(valueChanged(int)), this, SLOT(change_hw4(int)));
    QObject::connect( ui->horizontalSlider_5, SIGNAL(valueChanged(int)), this, SLOT(change_hw5(int)));
    QObject::connect( ui->horizontalSlider_6, SIGNAL(valueChanged(int)), this, SLOT(change_hw6(int)));
    QObject::connect( ui->horizontalSlider_7, SIGNAL(valueChanged(int)), this, SLOT(change_hw7(int)));
    QObject::connect( ui->horizontalSlider_8, SIGNAL(valueChanged(int)), this, SLOT(change_hw8(int)));
    QObject::connect( ui->horizontalSlider_9, SIGNAL(valueChanged(int)), this, SLOT(change_mid1(int)));
    QObject::connect( ui->horizontalSlider_10, SIGNAL(valueChanged(int)), this, SLOT(change_mid2(int)));
    QObject::connect( ui->horizontalSlider_11, SIGNAL(valueChanged(int)), this, SLOT(change_final(int)));
}

GradeCalculator::~GradeCalculator()
{
    delete ui;
}

void GradeCalculator::change_hw1(int value) {
    hw1_val = value;
    change_label();
    return;
}

void GradeCalculator::change_hw2(int value) {
    hw2_val = value;
    change_label();
    return;
}

void GradeCalculator::change_hw3(int value) {
    hw3_val = value;
    change_label();
    return;
}

void GradeCalculator::change_hw4(int value) {
    hw4_val = value;
    change_label();
    return;
}

void GradeCalculator::change_hw5(int value) {
    hw5_val = value;
    change_label();
    return;
}

void GradeCalculator::change_hw6(int value) {
    hw6_val = value;
    change_label();
    return;
}

void GradeCalculator::change_hw7(int value) {
    hw7_val = value;
    change_label();
    return;
}

void GradeCalculator::change_hw8(int value) {
    hw8_val = value;
    change_label();
    return;
}

void GradeCalculator::change_mid1(int value) {
    mid1_val = value;
    change_label();
    return;
}

void GradeCalculator::change_mid2(int value) {
    mid2_val = value;
    change_label();
    return;
}

void GradeCalculator::change_final(int value) {
    final_val = value;
    change_label();
    return;
}

void GradeCalculator::change_label() const {
    double mid1 = static_cast<double>(mid1_val);
    double mid2 = static_cast<double>(mid2_val);
    double final = static_cast<double>(final_val);
    // Find and remove lowest homework value
    std::vector<int> hw = {hw1_val, hw2_val, hw3_val, hw4_val, hw5_val, hw6_val, hw7_val, hw8_val};
    int min_hw = hw[0];
    int min_index = 0;
    for (int i = 1; i < 8; ++i) {
        if (hw[i] < min_hw) {
            min_hw = hw[i];
            min_index = i;
        }
    }
    hw.erase(hw.begin() + min_index);
    double hw_total = 0;
    for (int i = 0; i < 7; ++i) {
        hw_total += hw[i];
    }
    double max_midterm;
    if (mid1 > mid2) {
        max_midterm = mid1;
    } else {
        max_midterm = mid2;
    }
    double grade;
    QString text;
    // First grading scheme is default
    if (ui->radioButton_2->isChecked()) {
        // Second grading scheme
        grade = 100*(.25*(hw_total/700)+.3*(max_midterm/100)+.44*(final/100));
    } else {
        // First grading scheme
        grade = 100*(.25*(hw_total/700)+.2*(mid1/100)+.2*(mid2/100)+.35*(final/100));
    }
    text = QString::number(grade);
    ui->label_14->setText(text);
}
